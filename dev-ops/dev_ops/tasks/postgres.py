"""
PostgreSQL database backup helpers
"""
import os
from datetime import datetime

from invoke import task

from dev_ops.tasks.common import check_environ, create_temp_from_environ


@task
def dump_database(context):
    """
    Dump database into file
    :param context:
    """
    env = check_environ(
        dump_database.__name__,
        "DB_USERNAME",
        "DB_HOST",
        "DB_PORT",
        "DB_NAME",
        "DB_PASSWORD",
        "DUMP_FILENAME",
    )

    context.run(
        f"pg_dump "
        f"--create "
        f"--format t "
        f"--dbname {env['DB_NAME']} "
        f"--username {env['DB_USERNAME']} "
        f"--host {env['DB_HOST']} "
        f"--port {env['DB_PORT']} "
        f"--file {env['DUMP_FILENAME']} ",
        env={"PGPASSWORD": env["DB_PASSWORD"]},
        hide="stdout",
    )


@task
def send_dump(context):
    env = check_environ(
        send_dump.__name__, "SCP_USERNAME", "SCP_HOST", "SCP_PORT", "DUMP_FILENAME"
    )

    _, key_file_path = create_temp_from_environ(
        "SCP_KEY", caller=send_dump.__name__, prefix="scp_pkey."
    )

    context.run(
        f"scp "
        f"-i {key_file_path} "
        f"{env['DUMP_FILENAME']} "
        f"scp://{env['SCP_USERNAME']}@{env['SCP_HOST']}:{env['SCP_PORT']}/{datetime.now().isoformat()}.{env['DUMP_FILENAME']}"
    )

    os.remove(key_file_path)
