from invoke import Collection

from dev_ops.tasks import postgres

namespace = Collection()
namespace.add_collection(Collection.from_module(postgres))
