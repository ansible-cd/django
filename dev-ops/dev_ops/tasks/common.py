import os
from os.path import dirname
from pathlib import Path
from tempfile import mkstemp

__all__ = [
    "check_environ",
    "create_temp_from_environ",
    "PROJECT_ROOT",
]

PROJECT_ROOT = Path(dirname(__file__)).parent


def _format_list(source: list) -> str:
    if len(source) == 1:
        return source[0]
    else:
        last = source.pop(-1)
        return ", and ".join((", ".join(source), last))


def check_environ(caller: str, *variables: str):
    missed_variables = [
        variable for variable in variables if os.environ.get(variable) is None
    ]

    if missed_variables:
        plural = len(missed_variables) >= 2
        raise KeyError(
            f"Environment variable{'s' if plural else ''} {_format_list(missed_variables)}, "
            f"required by {caller}, {'are' if plural else 'is'} missed!"
        )

    return os.environ


def create_temp_from_environ(
    variable: str, *args, caller: str = None, **kwargs
) -> (int, str):
    env = check_environ(caller or create_temp_from_environ.__name__, variable)
    _, temp_path = mkstemp(*args, **kwargs, text=True)

    with open(temp_path, "w") as temp_file:
        temp_file.write(env[variable])

    return _, temp_path
