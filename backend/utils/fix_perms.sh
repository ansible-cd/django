#!/bin/bash

# this script allows fast permission fix after static media files upload.

case "$1" in
    "1")
        echo "Fixing permissions for development installation"
        docker-compose -f docker-compose.dev.yml exec -u root django chown django:django /app/static_django/ -Rf
        ;;
    "2")
        echo "Fixing permissions for staging installation"
        docker-compose -f docker-compose.staging.yml exec -u root django chown django:django /app/static_django/ -Rf
        ;;
    "3")
        echo "Fixing permissions for production installation"
        docker-compose -f docker-compose.production.yml exec -u root django chown django:django /app/static_django/ -Rf
        ;;
    *)
        echo "Usage:"
        echo "First argument is installation type, where:"
        echo "1 - development"
        echo "2 - staging"
        echo "3 - production"
        ;;
esac
