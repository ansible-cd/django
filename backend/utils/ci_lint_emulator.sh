#!/bin/bash

# This file is required to launch same CI steps as in CI/CD locally.
# This sometimes allows to solve CI problems faster.
docker-compose -f docker-compose.dev.yml up -d django
docker-compose -f docker-compose.dev.yml exec -T django pre-commit run -a -c .pre-commit-config-ci.yaml
