#!/bin/bash

# Deploy files from local init data to docker compose static container.

if [ ! -z "$1" ]; then
    docker cp "$1" backend_django_1:/app/static_django/
else
    docker cp app/init_data/files/staticfiles/* backend_django_1:/app/static_django/
fi
