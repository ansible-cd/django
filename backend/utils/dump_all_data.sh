#!/bin/bash

# Copy files from docker compose static container to local folder.

if [ ! -z "$1" ]; then
    mkdir -p -v "$1"
    docker cp backend_django_1:/app/static_django/ "$1"
    python manage.py dumpdata --format json -o "$1"/experience_request.json experience.ExperienceRequest
    python manage.py dumpdata --format json -o "$1"/experience.json experience.Experience
    python manage.py dumpdata --format json -o "$1"/user.json user
    python manage.py dumpdata --format json -o "$1"/user_block.json user_block
    python manage.py dumpdata --format json -o "$1"/messaging_thread.json messaging.MessagingThread
    python manage.py dumpdata --format json -o "$1"/messaging_thread_participants.json messaging.MessagingThreadParticipants
    python manage.py dumpdata --format json -o "$1"/messaging_message.json messaging.MessagingMessage
    python manage.py dumpdata --format json -o "$1"/messaging_message_state.json messaging.MessagingMessageState
    python manage.py dumpdata --format json -o "$1"/user_friends.json user_friends
    python manage.py dumpdata --format json -o "$1"/billing_billing_plans.json billing.BillingPlans
    python manage.py dumpdata --format json -o "$1"/billing_billing_events.json billing.BillingEvents
    python manage.py dumpdata --format json -o "$1"/billing_billing_user_details.json billing.BillingUserDetails
else
    echo "First argument should be folder"
fi
