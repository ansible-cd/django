#!/usr/bin/env python
"""Example of websocket client on python."""
import argparse
import asyncio
import json
import os
import sys
from pathlib import Path
from time import sleep

import django
from websockets import WebSocketCommonProtocol

project_dir = Path(__file__).parent.parent.resolve()
sys.path.append(str(project_dir))

os.chdir(str(project_dir))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.config.settings")
django.setup()

import websockets  # noqa
from channels.db import database_sync_to_async  # noqa
from django.contrib.auth import get_user_model  # noqa
from django.db import IntegrityError  # noqa
from rest_framework.authtoken.models import Token  # noqa


def parse_command_line_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description="Get command line options.")
    parser.add_argument(
        "--username",
        default="user1",
        type=str,
        help="Username from local test database to monitor from.",
    )
    parser.add_argument(
        "--host",
        default="localhost",
        type=str,
        help="Host to bind.",
    )
    return parser.parse_args()


User = get_user_model()


async def client_token_auth_headers(username):
    usr = await database_sync_to_async(User.objects.get)(username=username)
    try:
        token = await database_sync_to_async(Token.objects.create)(user=usr)
    except IntegrityError:
        token = await database_sync_to_async(Token.objects.get)(user=usr)
    headers = {"authorization": f"Token {token.key}"}
    return headers


class Client:
    def __init__(self):
        pass

    async def connect(self, host, username):
        """
        Connecting to webSocket server
        websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        """
        uri = f"ws://{host}/ws/"
        extra_headers = await client_token_auth_headers(username)
        self.connection = await websockets.client.connect(
            uri, extra_headers=extra_headers
        )
        if self.connection.open:
            print(
                f"Connection established. Details:" f"State: {self.connection.state}, "
            )
            return self.connection

    async def receiveMessage(self, connection: WebSocketCommonProtocol):
        """
        Receiving all server messages and handling them
        """
        while True:
            try:
                message = await connection.recv()
                print("Received message from server: " + str(message))
            except websockets.exceptions.ConnectionClosed:
                print(
                    f"Connection with server closed. Details:"
                    f"Code: {connection.close_code}, "
                    f"reason: {connection.close_reason}, "
                    f"state: {connection.state}, "
                )
                break

    async def heartbeat(self, connection):
        """
        Sending heartbeat to server every 5 seconds
        Ping - pong messages to verify connection is alive
        """
        while True:
            try:
                await connection.send(json.dumps({"ping": "ping"}))
                await asyncio.sleep(5)
            except websockets.exceptions.ConnectionClosed:
                print("Connection with server unexpectedly closed")
                break


if __name__ == "__main__":
    args = parse_command_line_args()

    def run_loop():
        # Creating client object
        client = Client()
        loop = asyncio.get_event_loop()

        # Start connection and get client connection protocol
        connection = loop.run_until_complete(client.connect(args.host, args.username))
        # Start listener and heartbeat
        tasks = [
            asyncio.ensure_future(client.heartbeat(connection)),
            asyncio.ensure_future(client.receiveMessage(connection)),
        ]

        loop.run_until_complete(asyncio.wait(tasks))

    while True:
        run_loop()
        print("=========================================")
        print("==== Auto restart on codebase change ====")
        print("=========================================")
        # Django and nginx need some time. Periodically.
        sleep(5)
