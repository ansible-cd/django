#!/bin/bash

# Copy files from docker compose static container to local folder.

if [ ! -z "$1" ]; then
    mkdir -p -v "$1"
    docker cp backend_django_1:/app/static_django/ "$1"
else
    echo "First argument should be folder"
fi
