"""
TeamCity Agent deploy helpers.
"""
import os

from invoke import task

from tasks.common import create_temp_from_environ


@task
def run_ansible(context, user, host, playbook):
    """
    Deploy changes to the generated inventory,
    using private key from the AGENT_PRIVATE_KEY env.

    :param user: Remote deploy user
    :param host: Remote address
    :param playbook: Ansible playbook to run
    """
    _, key_file_path = create_temp_from_environ(
        "DEPLOY_PRIVATE_KEY", caller=run_ansible.__name__, prefix="deploy_pkey."
    )

    context.run(
        f"ansible-playbook --inventory {host}, --user {user} -vv "
        f"--key-file {key_file_path} deploy/{playbook}"
    )

    os.remove(key_file_path)
