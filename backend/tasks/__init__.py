from invoke import Collection

from tasks import deploy

namespace = Collection()
namespace.add_collection(Collection.from_module(deploy))
