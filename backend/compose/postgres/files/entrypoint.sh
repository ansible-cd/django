#!/bin/bash
set -e

if [ "${USE_POSTGRES-1}" != "1" ]; then
    echo "### Local postgres is disabled. Just exit"
    exit 0
fi

# Launching normal init otherwise:
echo "### Local postgres is enabled. Launching normal init."
docker-entrypoint.sh postgres
