#!/bin/bash
set -e

if [ -f .env ] then
export $(cat .env | xargs)
fi

wait4x postgresql "$DJANGO_DATABASE_URL?sslmode=disable" --timeout 20s -- /bin/bash "$@"
