#!/bin/bash

python manage.py migrate;
python manage.py collectstatic --noinput;
gunicorn app.config.wsgi:application --bind 0.0.0.0:${APP_PORT-8000};
